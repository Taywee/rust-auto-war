# rust-auto-war

Simple Rust program that runs a single War card game.  Mostly a fun exercise.
Was actually more code than I expected, mostly around card management and
building robust-ish types.  Note how small the actual game loop is.  Might be
tweaked in the future to collect statistics.