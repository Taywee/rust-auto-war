extern crate rand;

use rand::thread_rng;
use rand::seq::SliceRandom;
use std::cmp;
use std::fmt;
use std::char;

pub fn deck_to_block(num: usize) -> char {
    match num {
        0 => ' ',
        1..=6 => '▁',
        7..=13 => '▂',
        14..=20 => '▃',
        21..=27 => '▄',
        28..=34 => '▅',
        35..=41 => '▆',
        42..=48 => '▇',
        _ => '█',
    }
}

#[derive(Clone, Copy)]
pub enum Suit {
    Spade,
    Heart,
    Diamond,
    Club
}

impl Suit {
    pub fn char(&self) -> char {
        match self {
            Suit::Spade => 'S',
            Suit::Heart => 'H',
            Suit::Diamond => 'D',
            Suit::Club => 'C',
        }
    }
    pub fn unicode_part(&self) -> u32 {
        match self {
            Suit::Spade => 0xA0,
            Suit::Heart => 0xB0,
            Suit::Diamond => 0xC0,
            Suit::Club => 0xD0,
        }
    }
}

#[derive(cmp::Eq)]
pub struct Value(u8);

impl Value {
    pub fn new(value: u8) -> Result<Value, String> {
        if value == 0 || value > 13 {
            Err(format!("Value {} out of range", value))
        } else {
            Ok(Value(value))
        }
    }
    pub fn char(&self) -> char {
        match self.0 {
            1 => 'A',
            10 => 'T',
            11 => 'J',
            12 => 'Q',
            13 => 'K',
            x => (x + ('0' as u8)) as char,
        }
    }

    fn unicode_part(&self) -> u32 {
        (match self.0 {
            x @ 1..=11 => x,
            x => x + 1,
        }) as u32
    }

    fn cmp_value(&self) -> u8{
        if self.0 == 1 {
            14
        } else {
            self.0
        }
    }
}

impl cmp::PartialEq for Value {
    fn eq(&self, other: &Self) -> bool {
        self.0 == other.0
    }
}

impl cmp::Ord for Value {
    fn cmp(&self, other: &Self) -> cmp::Ordering {
        self.cmp_value().cmp(&other.cmp_value())
    }
}

impl PartialOrd for Value {
    fn partial_cmp(&self, other: &Self) -> Option<cmp::Ordering> {
        Some(self.cmp(other))
    }
}

struct Card {
    suit: Suit,
    value: Value,
}

impl Card {
    const UNICODE_OFFSET: u32 = 0x1F000;
}

impl fmt::Display for Card {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "{}", char::from_u32(Self::UNICODE_OFFSET + self.suit.unicode_part() + self.value.unicode_part()).unwrap())
    }
}

struct Player {
    deck: Vec<Card>,
    discard: Vec<Card>,
}

impl fmt::Display for Player {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "{}+{}={}", deck_to_block(self.deck.len()), deck_to_block(self.discard.len()), deck_to_block(self.deck.len() + self.discard.len()))
    }
}

impl Player {
    /** Draw a card if possible.
     * This will shuffle if necessary.
     * If both deck and discard pile are empty, return None.
     */
    fn new(deck: Vec<Card>) -> Player {
        Player {
            deck: deck,
            discard: Vec::new(),
        }
    }

    fn draw(&mut self) -> Option<Card> {
        if self.deck.len() == 0 {
            let mut rng = thread_rng();
            self.deck.extend(self.discard.drain(..));
            self.deck.shuffle(&mut rng);
        }
        self.deck.pop()
    }

    fn discard(&mut self, card: Card) {
        self.discard.push(card);
    }

    fn card_count(&self) -> usize {
        self.deck.len() + self.discard.len()
    }
}

fn main() {
    let mut deck = Vec::new();
    for suit in &[Suit::Spade, Suit::Heart, Suit::Diamond, Suit::Club] {
        for value in 1..=13 {
            deck.push(Card{
                value: Value::new(value).unwrap(),
                suit: *suit,
            });
        }
    }

    {
        let mut rng = thread_rng();
        deck.shuffle(&mut rng);
    }

    let (first, second): (Vec<(usize, Card)>, Vec<(usize, Card)>) = deck.into_iter().enumerate().partition(|(i, _card)| i % 2 == 0);

    let mut player1 = Player::new(first.into_iter().map(|(_index, card)| card).collect());
    let mut player2 = Player::new(second.into_iter().map(|(_index, card)| card).collect());

    let mut booty = Vec::new();
    let mut round = 0;
    loop {
        round += 1;
        println!("Round {}", round);
        println!("P1: {}, P2: {}, Prize: {}", player1.card_count(), player2.card_count(), booty.len());
        let card1 = player1.draw();
        let card2 = player2.draw();
        match (card1, card2) {
            (Some(first), Some(second)) => {
                print!("{}{} v{} {}", player1, first, second, player2);
                if booty.len() > 0 {
                    print!(", Prize: ");
                    for card in &booty {
                        print!("{} ", card);
                    }
                }
                println!();
                if first.value == second.value {
                    println!("Tie, pushing into booty");
                    booty.push(first);
                    booty.push(second);
                } else if first.value > second.value {
                    println!("Player 1, wins {} cards", booty.len() + 2);
                    player1.discard(first);
                    player1.discard(second);
                    for card in booty {
                        player1.discard(card);
                    }
                    booty = Vec::new();
                } else {
                    println!("Player 2, wins {} cards", booty.len() + 2);
                    player2.discard(first);
                    player2.discard(second);
                    for card in booty {
                        player2.discard(card);
                    }
                    booty = Vec::new();
                }
            }
            (Some(_), None) => {
                println!("Player 1 won in {} turns!", round);
                break;
            }
            (None, Some(_)) => {
                println!("Player 2 won in {} turns!", round);
                break;
            }
            (None, None) => {
                panic!("Neither player had cards?  Something broke");
            }
        }
    }
}
